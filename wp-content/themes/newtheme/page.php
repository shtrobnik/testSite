<?php get_header(); ?>

<main role="main" class="container">
	<div class="row">
		<div class="col-md-8 blog-main">
			<?php
            get_template_part('template-parts/page/content', 'page');

			if (have_posts()) : while (have_posts()) : the_post();
				get_template_part('content', get_post_format());
			endwhile; endif;
			?>
		</div><!-- /.blog-main -->
	</div><!-- /.row -->

</main><!-- /.container -->
<?php get_footer();?>
