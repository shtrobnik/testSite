<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Airline</th>
            <th scope="col">Date</th>
            <th scope="col">Departure</th>
            <th scope="col">Arrival</th>
            <th scope="col">Time</th>
            <th scope="col">Price</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">
                <div class="info-wrapper row flex-nowrap justify-content-left align-items-center">
                    <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/vueling.png" alt="">
                    <div class="info-block">
                        <div>Vueling</div>
                        <div class="light-grey">VO1367</div>
                    </div>
                </div>
            </th>
            <td>
                <div class="info-block">
                    <div>03/11/2017</div>
                    <div class="light-grey">In 3 days</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Paris(CDG)</div>
                    <div class="light-grey">08:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Miami</div>
                    <div class="light-grey">05:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>08:45</div>
                    <div class="light-grey">CDG - MIA</div>
                </div>
            </td>
            <td>
                <a href="#" class="btn"> 1500 $</a>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <div class="info-wrapper row flex-nowrap justify-content-left align-items-center">
                    <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/air-france.png" alt="">
                    <div class="info-block">
                        <div>Air France</div>
                        <div>AF90</div>
                    </div>
                </div>
            </th>
            <td>
                <div class="info-block">
                    <div>03/11/2017</div>
                    <div class="light-grey">In 3 days</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Paris(CDG)</div>
                    <div class="light-grey">08:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Miami</div>
                    <div class="light-grey">05:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>08:45</div>
                    <div class="light-grey">CDG - MIA</div>
                </div>
            </td>
            <td>
                <a href="#" class="btn"> 1500 $</a>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <div class="info-wrapper row flex-nowrap justify-content-left align-items-center">
                    <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/american.png" alt="">
                    <div class="info-block">
                        <div>American</div>
                        <div>AA63</div>
                    </div>
                </div>
            </th>
            <td>
                <div class="info-block">
                    <div>03/11/2017</div>
                    <div class="light-grey">In 3 days</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Paris(CDG)</div>
                    <div class="light-grey">08:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Miami</div>
                    <div class="light-grey">05:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>08:45</div>
                    <div class="light-grey">CDG - MIA</div>
                </div>
            </td>
            <td>
                <a href="#" class="btn"> 1500 $</a>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <div class="info-wrapper row flex-nowrap justify-content-left align-items-center">
                    <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/delta.png" alt="">
                    <div class="info-block">
                        <div>Delta</div>
                        <div>DL405</div>
                    </div>
                </div>
            </th>
            <td>
                <div class="info-block">
                    <div>03/11/2017</div>
                    <div class="light-grey">In 3 days</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Paris(CDG)</div>
                    <div class="light-grey">08:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Miami</div>
                    <div class="light-grey">05:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>08:45</div>
                    <div class="light-grey">CDG - MIA</div>
                </div>
            </td>
            <td>
                <a href="#" class="btn"> 1500 $</a>
            </td>
        </tr>
        <tr>
            <th scope="row">
                <div class="info-wrapper row flex-nowrap justify-content-left align-items-center">
                    <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/transavia.png" alt="">
                    <div class="info-block">
                        <div>Transavia</div>
                        <div>TO4330</div>
                    </div>
                </div>
            </th>
            <td>
                <div class="info-block">
                    <div>03/11/2017</div>
                    <div class="light-grey">In 3 days</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Paris(CDG)</div>
                    <div class="light-grey">08:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>Miami</div>
                    <div class="light-grey">05:30</div>
                </div>
            </td>
            <td>
                <div class="info-block">
                    <div>08:45</div>
                    <div class="light-grey">CDG - MIA</div>
                </div>
            </td>
            <td>
                <a href="#" class="btn"> 1500 $</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>