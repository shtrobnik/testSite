<ul class="hotels-l">
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel1.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel Paris</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">12 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">30 $</a>
        </div>
    </li>
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel2.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel Elfel</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">8 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">31 $</a>
        </div>
    </li>
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel3.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel Dnipro</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">2 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">30 $</a>
        </div>
    </li>
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel4.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel Dubai</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">120 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">27 $</a>
        </div>
    </li>
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel5.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel Kiev</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">20 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">37 $</a>
        </div>
    </li>
    <li class="hotels-l-i">
        <figure><img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel6.jpg" alt=""></figure>
        <div class="description">
            <p class="hotel-info">Hotel London</p>
            <div class="hotel-info nav d-flex justify-content-between">
                <span class="light-grey">30 km</span>
                <span class="light-grey">wifi</span>
            </div>
            <a href="#" class="btn">45 $</a>
        </div>
    </li>
</ul>