
<ul class="trips-l">
    <li class="trips-l-i">
        <figure class="trips-image">
            <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel1.jpg" alt="">
        </figure>
        <div class="description">
            <p class="title">Lorem ipsum dolor sit</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur cupiditate, eligendi enim eos error fugit hic ipsam numquam odit perferendis quibusdam quisquam quod quos ratione rem repellendus velit vitae?</p>
        </div>
    </li>
    <li class="trips-l-i">
        <figure class="trips-image">
            <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel2.jpg" alt="">
        </figure>
        <div class="description">
            <p class="title">Lorem ipsum dolor sit</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur cupiditate, eligendi enim eos error fugit hic ipsam numquam odit perferendis quibusdam quisquam quod quos ratione rem repellendus velit vitae?</p>
        </div>
    </li>
    <li class="trips-l-i">
        <figure class="trips-image">
            <img src="<?php echo get_bloginfo('template_directory');?>/assets/images/hotel5.jpg" alt="">
        </figure>
        <div class="description">
            <p class="title">Lorem ipsum dolor sit</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur cupiditate, eligendi enim eos error fugit hic ipsam numquam odit perferendis quibusdam quisquam quod quos ratione rem repellendus velit vitae?</p>
        </div>
    </li>
</ul>