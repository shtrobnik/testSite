<?php get_header(); ?>

<main role="main" class="container">
    <div class="row">
        <div class="blog-main w-100">
            <section class="section-info overflow-hidden">
                <header class="row flex-nowrap justify-content-between align-items-center">
                    <h2 id="flights">Flights</h2>
                    <?php
                        $args = array(
                            'menu' => 'flightsMenu',
                            'container' => 'nav',
                            'container_class' => 'nav small-menu',
                            'menu_class' => 'nav d-flex justify-content-between',
                            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                            'depth' => 0,
                        );
                        wp_nav_menu($args);
                    ?>
                </header>

                <?php get_template_part('table');?>

                <a href="#" class="show-more light-grey float-right">See all flights</a>
            </section>
            <section class="section-info overflow-hidden">
                <header class="row flex-nowrap justify-content-between align-items-center">
                    <h2 id="hotels">Hotels</h2>
                    <?php
                        $args = array(
                            'menu' => 'hotelsMenu',
                            'container' => 'nav',
                            'container_class' => 'nav small-menu',
                            'menu_class' => 'nav d-flex justify-content-between',
                            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                            'depth' => 0,
                        );
                        wp_nav_menu($args)
                    ?>
                </header>

                <?php get_template_part('hotels');?>

                <a href="#" class="show-more light-grey float-right">See all hotels</a>
            </section>

            <section class="section-info overflow-hidden">
                <header class="row flex-nowrap justify-content-between align-items-center">
                    <h2 id="hotels">Attractions</h2>
                    <?php
                        $args = array(
                            'menu' => 'attractionsMenu',
                            'container' => 'nav',
                            'container_class' => 'nav small-menu',
                            'menu_class' => 'nav d-flex justify-content-between',
                            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                            'depth' => 0,
                        );
                        wp_nav_menu($args)
                    ?>
                </header>

                <?php echo do_shortcode('[gallery size="full" include="103,104,105,106,107,108" columns=3]'); ?>

                <a href="#" class="show-more light-grey float-right">See all attractions</a>
            </section>

            <section class="section-info overflow-hidden">
                <header class="row flex-nowrap justify-content-between align-items-center">
                    <h2 id="hotels">Trips</h2>
                    <?php
                        $args = array(
                            'menu' => 'tripsMenu',
                            'container' => 'nav',
                            'container_class' => 'nav small-menu',
                            'menu_class' => 'nav d-flex justify-content-between',
                            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                            'depth' => 0,
                        );
                        wp_nav_menu($args)
                    ?>
                </header>

                <?php get_template_part('trips');?>

                <a href="#" class="show-more light-grey float-right">See all trips</a>
            </section>
			<?php
			if (have_posts()) : while (have_posts()) : the_post();
				get_template_part('content', get_post_format());
			endwhile; endif;
			?>
        </div><!-- /.blog-main -->
    </div><!-- /.row -->

</main><!-- /.container -->
<?php get_footer();?>
