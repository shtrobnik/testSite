<footer class="blog-footer">
    <div class="container">
        <section class="footer-links-wrap row flex-nowrap justify-content-between align-items-top">
            <div class="footer-links">
                <h2>Cities</h2>
                <?php
                $args = array(
                    'menu' => 'citiesMenu',
                    'container' => 'nav',
                    'container_class' => 'footer-menu',
                    'menu_class' => '',
                    'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                    'depth' => 0,
                );
                wp_nav_menu($args)
                ?>
            </div>
            <div class="footer-links">
                <h2>Explorer</h2>
                <?php
                $args = array(
                    'menu' => 'explorerMenu',
                    'container' => 'nav',
                    'container_class' => 'footer-menu',
                    'menu_class' => '',
                    'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                    'depth' => 0,
                );
                wp_nav_menu($args)
                ?>
            </div>
            <div class="footer-links">
                <h2>About Us</h2>
                <?php
                $args = array(
                    'menu' => 'aboutUsMenu',
                    'container' => 'nav',
                    'container_class' => 'footer-menu',
                    'menu_class' => '',
                    'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                    'depth' => 0,
                );
                wp_nav_menu($args)
                ?>
            </div>
            <div class="footer-links">
                <h2>Contact</h2>
                <p>Feel free to get in touch via email:</p>
                <a href="#">support@travelcream.com</a>
            </div>
        </section>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<?php wp_footer();?>
</body>
</html>