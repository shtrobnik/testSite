<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Jekyll v3.8.6">
	<title>Blog Template · Bootstrap</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/blog/">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<!-- Favicons -->
	<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
	<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
	<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
	<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
	<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
	<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
	<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#563d7c">


	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>
	<!-- Custom styles for this template -->
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo get_bloginfo('template_directory');?>/style.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo('template_directory');?>/assets/css/main.css" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body>
	<header class="blog-header py-3">
        <div class="container">
            <div class="row flex-nowrap justify-content-between align-items-center header-wrap">
                <div class="col-3 text-center logo-wrap">
                    <a class="blog-header-logo text-white" href="<?php echo get_bloginfo('wpurl');?>"><?php echo get_bloginfo('name');?></a>
                </div>
                <div class="col-5 menu-wrap">
                    <div class="nav-scroller">
                        <?php
                        $args = array(
                            'menu' => 'mainMenu',
                            'container' => 'nav',
                            'container_class' => 'nav d-flex justify-content-between',
                            'menu_class' => 'nav d-flex justify-content-between',
                            'echo' => true,
                            'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
                            'depth' => 0,
                        );
                        function wp_nav_menu_remove_attributes( $menu ){
                            return $menu = preg_replace('/ id=\"(.*)\" class=\"(.*)\"/iU', '', $menu );
                        }
                        add_filter( 'wp_nav_menu', 'wp_nav_menu_remove_attributes' );
                        wp_nav_menu($args)
                        ?>
                    </div>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center btn-wrap">
                    <a class="btn btn-sm btn-outline-secondary text-white" href="#">Sign up</a>
                </div>
            </div>
            <div class="row d-flex justify-content-center align-items-center form-wrap">
                <form class="form-inline d-flex justify-content-center">
                    <label class="sr-only" for="inlineFormInputHome">Where are you living from?</label>
                    <input type="text" class="form-control col-4" id="inlineFormInputHome" placeholder="Where are you living from?">

                    <label class="sr-only" for="inlineFormInputPath">Where do you want to go?</label>
                    <input type="text" class="form-control col-4" id="inlineFormInputPath" placeholder="Where do you want to go?">

                    <label class="sr-only" for="inlineFormInputGroupDate">Date</label>
                    <input type="text" class="form-control col-2 date-item" id="inlineFormInputGroupDate" placeholder="Date">

                    <button type="submit" class="btn form-control text-white">Search</button>
                </form>
            </div>
        </div>
	</header>