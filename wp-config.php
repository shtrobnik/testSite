<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'test_site' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Bnk;o#HBVFF!v.K^]q$=;s7DnKT%tG.`oKVsO$[oJ;A%2?:7K*L0l_z(!an!tUU<' );
define( 'SECURE_AUTH_KEY',  '$6-7`U Y{vlm}u-t?%?&LdD0{Xs9a[[iSQFY{hULC*YxMQ#&y {Eb3;sk,09sr;N' );
define( 'LOGGED_IN_KEY',    'Yj9g2$TKKuK`BvCMgd@Y_tc?tP[#$gGaV5Q&{W4]iFKMbV0:XOO5UrG6dLm$~8qO' );
define( 'NONCE_KEY',        '6%%||:5azV1BfyJ7)OEf]2j(Vw~CXT0AyxwS)zJfwoz~47=uftL%%{nBe`XiTk0:' );
define( 'AUTH_SALT',        'P~::Hm=5O<0pVjuJ@,(il*)_#GezZa_c;:G?Y{_,4[!(&X%GG]@of?}iT.jR/Fv/' );
define( 'SECURE_AUTH_SALT', '+maU|_Lmm~C>Y4[k6*1c~E|#z?_2z(Him.pZEOu_l(B7LTJ]w]y0XEr|)a}eCByY' );
define( 'LOGGED_IN_SALT',   ',b#DSe,VjWwLf(MyBfV{>P0mHq.Pgw+&qh?ZgGwtup !LJ%vSWGhRSwqUsZy2edA' );
define( 'NONCE_SALT',       'JR8*Si]O<&GuOj4/H>tN5wUt_tblG_TC|WgwTbK9 0(HL]X[^KZVAvHg16.21[sW' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
